class Mult extends Operation{
	public int operate (int a, int b){
		return a*b;
	}
}

class Div extends Operation{
	public int operate (int a, int b){
		return a/b;
	}
}

class Arroba extends Operation{
	public int operate (int a, int b){
		return b*(a-2)/(a+b);
	}
}

public class Client{

	public static void main (String  [] args){
	Calc calc = new Calc();
	calc.register("*",new Mult());
	calc.register("/",new Div());
	calc.register("@",new Arroba());
	int result = calc.operate("@",5,9);;
	System.out.println(result);
	}
}


